# Micro-Shell

### Partie 1
1/ En Rust les références permettent de définir l'emprunt d'une valeur donnée

2/ Rust est compilé nativement, sans machine virtuelle

3/ La valeur maximale addressable est 255 / 0xFF 

### Partie 1.1
4/Récupérer le status permet de contrôler le bon déroulement du programme et gérer les erreurs

5/Pendant que l'enfant s'execute, le programme attend

### Installation
Clôner le repository

```sh
$ git clone https://gitlab.com/demirel.sc/micro-shell.git
```

Compiler et lancer le programme

```sh
$ cargo build
$ cargo run
```

### Commands

Quitter le programme
```sh
> exit
> quit
```
