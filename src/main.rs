use std::io::{self, Write};
use std::process::Command;
use regex::Regex;

fn main() -> io::Result<()>{
    let stdin = io::stdin();
    let stdout = io::stdout();
    // Regex pour la séparation des commandes et arguments
    let regex = Regex::new(r"(?m)[0-9a-zA-Z-/><&|.~]+").unwrap();
    loop {
        {
            let mut handle = stdout.lock();
            handle.write_all(b"> ")?;
            handle.flush()?
        }
        let mut user_input = String::with_capacity(256);
        stdin.read_line(&mut user_input)?;
        // Récupération de l'entrée utilisateur
        let user_input = user_input.trim_end();
        let mut parsed_user_input = Vec::new();
        // Récupération et parsing de l'entrée dans un vecteur
        parsed_user_input = regex.find_iter(user_input).map(|elem| elem.as_str()).collect();
        if !parsed_user_input.is_empty() {
            // Commandes pour quitter le shell
            if parsed_user_input[0] == "exit" || parsed_user_input[0] == "quit"{
                println!("Successfully exited");
                break;
            }
            if parsed_user_input.len() == 1 {
                // Commande simple sans arguments
                let status = Command::new(parsed_user_input[0])
                    .status();
                if status.is_err() {
                    println!("Error command : {:?} failed", user_input);
                }
            }
            else {
                // Commande avec arguments
                let mut args = Vec::new();
                for i in 1..parsed_user_input.len() {
                    args.push(parsed_user_input[i]);
                }
                let status = Command::new(parsed_user_input[0])
                    .args(args)
                    .status();
                if status.is_err() {
                    println!("Error command : {:?} failed", user_input);
                }
            }
        }
    }
    Ok(())
}
